##' Creates a ClientExtension object
##'
##' @title Client Extensions Model
##' @param id character, client identifier
##' @param tag chatacter, client tags
##' @param comment character, client comment
##' @return ClientExtensions, list containing the specified client extensions
##' @author Filippo Monari
##' @export
ClientExtensions <- function (id="NULL", tag="NULL", comment="NULL") {
    obj = list("id"=id, "tag"=tag, "comment"=comment)
    class(obj) = c("ClientExtensions", "list")
    obj
}
##' Creates a Double object
##'
##' @title Double Model
##' @param x double
##' @return Double, a character representing the given double with 4 decimal places
##' @author Filippo Monari
##' @export
Double <- function (x) {
    obj = as.character(signif(x, 4))
    class(obj) = c("Double", class(obj))
    obj
}
