
## Accounts








## Trades
list_trades <- function (account_id) .get("accounts", account_id, "trades")

list_open_trades <- function (account_id) .get("accounts", account_id, "openTrades")

get_trade_details <- function (account_id, trade_id) .get("accounts", account_id, "trades", trade_id)

close_trade <- function (units, account_id, trade_id) .post(units, "accounts", account_id, "trades", trade_id, "close")

update_order_client_extensions <- function (ext, account_id, trade_id) .put(ext, "accounts", account_id, "trades",
                                                                            trade_id, "clientExtensions")

manage_trade_orders <- function (body, account_id, trade_id) .put(body, "accounts", account_id, "trades", trade_id, "orders")








## Pricing
get_latest_candles <- function (account_id) .get("accounts", account_id, "candles", "latest")

get_pricing <- function (account_id) .get("accounts", account_id, "pricing")

stream_prices <- function (account_id) .get("accounts", account_id, "pricing", "stream")

get_instruemnt_candles <- function (account_id, instruemnt) .get("accounts", account_id, "instruments", instrument, "candles")

